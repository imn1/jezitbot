# Jezitbot

# How to build (armv7)

- requires [cross-rs](https://github.com/cross-rs/cross)

```
$ cargo clean
$ cross build --release --target armv7-unknown-linux-gnueabihf
```

# How to deploy

```
$ ansible-playbook -K deploy.yaml
```

# Possible commands

- `/dp` - to get the departures
- `/rec` - to get all recipes
- `/rec chicken` - to search all "chicken" recipes
