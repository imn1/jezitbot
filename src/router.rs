use std::sync::Arc;

use teloxide::prelude::*;
use teloxide::utils::command::BotCommands;

use crate::cmds::help;
use crate::cmds::recipes;
use crate::cmds::trs;

pub struct RouterPayload {
    pub bot: Bot,
    pub msg: Message,
    pub cmd: Command,
}

#[derive(BotCommands, Clone, PartialEq, Debug)]
#[command(rename_rule = "lowercase")]
pub enum Command {
    #[command(description = "off")]
    Start,
    #[command(description = "shows this help message")]
    Help,
    #[command(description = "finds out nearest departures")]
    Dp,
    #[command(description = "finds recipe documents")]
    Rec(String),
}
pub async fn router(bot: Bot, msg: Message, cmd: Command) -> ResponseResult<()> {
    let payload = Arc::new(RouterPayload {
        bot,
        msg,
        cmd: cmd.clone(),
    });
    let result = match cmd {
        Command::Dp => trs(payload.clone()).await,
        Command::Rec(text) => recipes(payload.clone(), text).await,
        Command::Help | Command::Start => help(payload.clone()).await,
    };

    // In case of error - let's print out for now.
    // TODO: better logging.
    // TODO: returning an error log back via telegram message.
    if let Err(e) = result {
        error!("{}", e);
    }

    Ok(())
}
