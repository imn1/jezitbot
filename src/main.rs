use teloxide::prelude::*;
mod cmds;
mod router;
#[macro_use]
extern crate log;

const TOKEN: &str = "6949466751:AAE1-OkQ1Ufie_cOkp5HbXPs47Pc8KDwiec";

#[tokio::main]
async fn main() {
    pretty_env_logger::init();
    let bot = Bot::new(TOKEN);

    info!("Stargint bot ...");

    // teloxide::repl(bot, |bot: Bot, msg: Message| async move {
    //     bot.send_dice(msg.chat.id).await?;
    //     Ok(())
    // }).await

    router::Command::repl(bot, router::router).await;
}
