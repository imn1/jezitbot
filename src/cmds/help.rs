use std::sync::Arc;

use teloxide::prelude::*;
use teloxide::utils::command::BotCommands;

use crate::router::Command;
use crate::router::RouterPayload;
use anyhow::Result;

pub async fn help(payload: Arc<RouterPayload>) -> Result<()> {
    payload
        .bot
        .send_message(payload.msg.chat.id, Command::descriptions().to_string())
        .await?;

    Ok(())
}
