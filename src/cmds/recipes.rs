use anyhow::Result;
use diacritics::remove_diacritics;
use std::sync::Arc;
use teloxide::{requests::Requester, types::InputFile};
use tokio::fs::{read_dir, DirEntry};

use crate::router::RouterPayload;
const RECIPES_PATH: &str = "/media/data/docs/recipes";
// const RECIPES_PATH: &str = "/home/n1/docs/recipes";

pub async fn recipes(payload: Arc<RouterPayload>, text: String) -> Result<()> {
    let mut stream = read_dir(RECIPES_PATH).await?;
    let mut pdf_files = vec![];

    // Load all pdf files.
    while let Some(record) = stream.next_entry().await? {
        if record.file_type().await?.is_file() && "pdf" == record.path().extension().unwrap() {
            pdf_files.push(record);
        }
    }

    // Search the query.
    let query = remove_diacritics(&text);
    let found_files = pdf_files
        .iter()
        .filter(|i| {
            // partial match inc. spaces
            i.path()
                .file_stem()
                .unwrap()
                .to_string_lossy()
                .replace('_', " ")
                .contains(&query)
                // exact file name match
                || i.path().file_name().unwrap().to_string_lossy().eq(&query)
        })
        .collect::<Vec<&DirEntry>>();

    match found_files.len() {
        // No files has been found.
        0 => {
            payload
                .bot
                .send_message(payload.msg.chat.id, "No recipes has been found.")
                .await?;
        }
        // Only one file has been found -> send right away.
        1 => {
            payload
                .bot
                .send_document(
                    payload.msg.chat.id,
                    InputFile::file(found_files.first().unwrap().path()),
                )
                .await?;
        }
        // Multiple files has been found -> send a list of found files.
        _ => {
            let mut output = format!("These {} files has been found:\n", found_files.len());

            for f in found_files {
                output.push_str(
                    format!("- {}\n", f.path().file_name().unwrap().to_string_lossy()).as_str(),
                );
            }

            output.push_str("\n\nEnter more specific query or exact file name.");

            payload
                .bot
                .send_message(payload.msg.chat.id, output)
                .await?;
        }
    };

    Ok(())
}
