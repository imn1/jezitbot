use std::sync::Arc;

use crate::router::RouterPayload;
use anyhow::anyhow;
use anyhow::Result;
use chrono::Local;
use teloxide::payloads::SendMessageSetters;
use teloxide::requests::Requester;
use teloxide::types::ParseMode;
use teloxide::utils::markdown::{bold, escape};
use trs::ui::Ui;
use trs::Config;
use trs::Timetables;
use trs::UiConfig;

pub async fn trs(payload: Arc<RouterPayload>) -> Result<()> {
    let limit = 3;
    let config = Arc::new(
        Config::load()
            .await
            .map_err(|_| anyhow!("Couldn't load trs config."))?,
    );
    let ui = Ui::new(UiConfig { limit });
    let timetables = Timetables::new(config).await;
    let mut departures = timetables.get_departures(limit);
    let mut output = String::new();

    dbg!(&departures);

    departures.sort_by(|a, b| a.stop.name.partial_cmp(&b.stop.name).unwrap());

    for mut dep in departures {
        // Header.
        output.push_str(
            bold(
                escape(format!("{} » {}\n", &dep.stop.name, &dep.stop.terminating_stop).as_str())
                    .as_str(),
            )
            .as_str(),
        );

        // Add "prague" feature details.
        dep.spice_up_additionals().await;

        // Departures.
        for departure_record in &dep.get_departures_in_regard_delays()[..limit] {
            if departure_record.stop_time.is_some() {
                let st_adjusted = departure_record.get_stop_time_adjusted();

                output.push_str(
                    format!(
                        "{}\n",
                        escape(
                            format!(
                                "{} - {}   (in {} min)   {}",
                                departure_record.route,
                                st_adjusted.stop_time.format("%H:%M"),
                                departure_record
                                    .get_relative_departure(Local::now())
                                    .num_minutes(),
                                ui.get_additionals(departure_record)
                            )
                            .as_str()
                        )
                    )
                    .as_str(),
                )
            }
        }

        // Separate stops.
        output.push('\n');
    }

    payload
        .bot
        .send_message(payload.msg.chat.id, output)
        .parse_mode(ParseMode::MarkdownV2)
        .await?;

    Ok(())
}
